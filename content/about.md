## About

### FeralSec

FeralSec are four spooky pals based in Australia, who all care about privacy, security, empathy & anti-capitalism (amongst other special interests). We chat to folks across the security, privacy and technology realm with the goal of sharing knowledge and tools.

Whilst we're not going to cover all of the below topics in each stream, and we might not talk about any of these things directly at all, here's an an idea of who we are and what we care about:

* Bringing [empathy](https://en.wikipedia.org/wiki/Empathy) and [anti-authoritarian](https://en.wikipedia.org/wiki/Anti-authoritarianism) thinking to what we do
* Building and sharing tools which can be used to build more human processes for protecting the [digital rights](https://en.wikipedia.org/wiki/Digital_rights), security and privacy of users
* Destroying [paternalism](https://en.wikipedia.org/wiki/Paternalism) when it comes to security & privacy - meeting people where they are, not where we think they should be
* Building better representation for [under-represented groups](https://en.wikipedia.org/wiki/Underrepresented_group) in our communities and industry
* Perfectionism, defensiveness and individual ego are the enemy of inclusivity and good decision making in teams & communities
* The security & privacy [hype-train](https://en.wiktionary.org/wiki/hype_train) is harmful and we should work to reframe security & privacy around reality and our shared humanity
* Sharing knowledge and tooling across our communities in a way that is interesting and fun, and relevant
* Dismantling [white supremacy culture](https://www.whitesupremacyculture.info/uploads/4/3/5/7/43579015/okun_-_white_sup_culture_2020.pdf)
* Building sustainable and intentional computing practices which respect user autonomy amidst a world of [climate change](https://en.wikipedia.org/wiki/Human_impact_on_the_environment#Climate_change) and [surveillance capitalism](https://en.wikipedia.org/wiki/Surveillance_capitalism)
* Information should be free and [DRM](https://en.wikipedia.org/wiki/Digital_rights_management) sucks

Some topics we do not spend time talking about because we do not find them valuable:
* Blockchain & cryptocurrency
* Whether or not your website needs an A+ rating on your TLS certificates
* Industry hot-takes & InfoSec celebrities
* Which Linux distribution is the best

We're aware that the nature of industry, work and our work is inherently political, and we have the privilege of working in an industry and time where we can be open about our politics and how we approach this stream, this community, and our work. 

We're realistically not going to smash the state or surveillance capitalism overnight - we (and many of our guests) spend a lot of our time propping up capitalism as part of our day jobs. Many of our streams will discuss topics which will be aimed at being useful in the context of working in the security or privacy industry. We always do our best however to bring ourselves and our beliefs to those discussions and our work.

### The Team

* attacus / Lilly (she/her)
* errbufferoverfl / Bec (they/them)
* p0kemina / Mimi (she/her)
* Rayne (they/them)

### Want to be on the show?

We have a whole [page on how to submit a talk proposal a be a guest on the show](/become-a-guest)!

### Why FeralSec?

It sounds pretty cool, and we're all feral little gremlins, and we bring that energy to everything we do.

### FeralTV

* [Twitch Live-stream](https://twitch.tv/feralsec)
* [Past Show Recordings](https://youtube.com/@FeralSec)




