Code of Conduct
==========================

*Last Updated 05 Jun 2023*

---
### Table of Contents

*   [Short Version](#CodeofConduct-ShortVersion)
*   [Long Version](#CodeofConduct-LongVersion)
    *   [Our Standards](#CodeofConduct-OurStandards)
    *   [Enforcement Responsibilities](#CodeofConduct-EnforcementResponsibilities)
    *   [Enforcement Guidelines](#CodeofConduct-EnforcementGuidelines)
        *   [1\. Correction](#CodeofConduct-1.Correction)
        *   [2\. Warning](#CodeofConduct-2.Warning)
        *   [3\. Temporary Ban](#CodeofConduct-3.TemporaryBan)
        *   [4\. Permanent Ban](#CodeofConduct-4.PermanentBan)
    *   [Reporting](#CodeofConduct-Reporting)
        *   [Where to report incidents](#CodeofConduct-Wheretoreportincidents)
        *   [Guidelines for reporting incidents](#CodeofConduct-Guidelinesforreportingincidents)
        *   [Code of Conduct Active Response Ensurers](#CodeofConduct-CodeofConductActiveResponseEnsurers)

---

Short Version
=============

We love to be a little feral, but that doesn't excuse harassing, hurtful, brandish, or offensive behavior. We prioritize marginalized groups, impact over intent and moderate with respect.

We as members, contributors, and leaders pledge to make participation in our community a harassment-free experience for everyone, regardless of age, body size, visible or invisible disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socioeconomic status, nationality, personal appearance, race, religion, or sexual identity and orientation.

We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive, and healthy community.

Long Version
============

We love to be a little feral, but that doesn't excuse harassing, hurtful, brandish, or offensive behavior. We prioritize marginalized groups, impact over intent and moderate with respect.

We as members, contributors, and leaders pledge to make participation in our community a harassment-free experience for everyone, regardless of age, body size, visible or invisible disability, ethnicity, sex characteristics, gender identity and expression, level of experience, education, socio-economic status, nationality, personal appearance, race, religion, or sexual identity and orientation.

We pledge to act and interact in ways that contribute to an open, welcoming, diverse, inclusive, and healthy community.

### Our Standards

Examples of behavior that contributes to a positive environment for our community include:

*   Demonstrating empathy and kindness toward other people
    
*   Being respectful of differing opinions, viewpoints, and experiences
    
*   Giving and gracefully accepting constructive feedback given in good faith
    
*   Accepting responsibility and apologizing to those affected by our mistakes, and learning from the experience
    
*   Focusing on what is best not just for us as individuals, but for the overall community
    
*   Showing consideration toward the accessibility needs of other community members, for example by using “alt” text when sharing visual media
    
*   Using the preferred pronouns of community members, guests & hosts, and sharing your preferred pronouns
    

Examples of unacceptable behavior include:

*   The use of sexualized language or imagery, and sexual attention or advances of any kind
    
*   Trolling, insulting or derogatory comments, and personal or political attacks
    
*   Public or private harassment
    
*   Publishing others’ private information, such as a physical or email address, without their explicit permission
    
*   The use of racialized insults or perpetuation of racial stereotypes
    
*   The deliberate misgendering of community members
    
*   Other conduct which could reasonably be considered inappropriate in a professional setting
    

Additionally supremacy in the form of fascism, [white supremacy](https://www.r2hub.org/library/overt-and-covert-racism), colonialism or religious extremism are not welcome. This includes:

*   Overt and covert forms of [white supremacy](https://www.r2hub.org/library/overt-and-covert-racism).
    
*   Supporting violence towards the participants of a religion.
    
*   Spreading hateful speech regarding religion is not tolerated.
    
*   Supporting or spreading support of one religion being “dominant” over others
    
*   Supporting or spreading support for religiously motivated hate speech or violence
    
*   Attacking / demeaning others’ sexual orientation
    
*   Attacking / demeaning others' gender expression
    
*   Supporting violence towards people on the basis of sexual orientation or gender expression
    
*   Spreading hateful speech regarding sexual orientation or gender expression
    
*   Supporting or spreading support indicating one sexual orientation as “the only correct” one, or “only natural” one, etc. or any discussions of essentialist views of gender such as “biological sex”, especially relating to community members
    
*   Supporting or spreading support for hate speech or violence against a specific sexual orientation or gender
    

### Enforcement Responsibilities

The FeralSec Crew are responsible for clarifying and enforcing our standards of acceptable behavior and will take appropriate and fair corrective action in response to any behavior that they deem inappropriate, threatening, offensive, or harmful.

The FeralSec Crew have the right and responsibility to remove, edit, or reject comments, commits, code, wiki edits, issues, and other contributions that are not aligned to this Code of Conduct, and will communicate reasons for moderation decisions when appropriate.

The FeralSec Crew have the right and responsibility to eject members, either temporarily or permanently, from community spaces as deemed necessary to maintain the standards & code of conduct of the community, and protect other community members from the actions of individuals violating this code of conduct.

### Enforcement Guidelines

FeralSec staff will follow these Community Impact Guidelines in determining the consequences for any action they deem in violation of this Code of Conduct:

#### 1\. Correction

**Community Impact**: Use of inappropriate language or other behavior deemed unprofessional or unwelcome in the community.

**Consequence**: A private, written warning from The FeralSec Crew, providing clarity around the nature of the violation and an explanation of why the behavior was inappropriate. A public apology may be requested.

#### 2\. Warning

**Community Impact**: A violation through a single incident or series of actions.

**Consequence**: A warning with consequences for continued behavior. No interaction with the people involved, including unsolicited interaction with those enforcing the Code of Conduct, for a specified period of time. This includes avoiding interactions in community spaces as well as external channels like social media. Violating these terms may lead to a temporary or permanent ban.

#### 3\. Temporary Ban

**Community Impact**: A serious violation of community standards, including sustained inappropriate behavior.

**Consequence**: A temporary ban from any sort of interaction or public communication with the community for a specified period of time. No public or private interaction with the people involved, including unsolicited interaction with those enforcing the Code of Conduct, is allowed during this period. Violating these terms may lead to a permanent ban.

#### 4\. Permanent Ban

**Community Impact**: Demonstrating a pattern of violation of community standards, including sustained inappropriate behavior, harassment of an individual, or aggression toward or disparagement of classes of individuals.

**Consequence**: A permanent ban from any sort of public interaction within the project community.

### Reporting

#### Where to report incidents

If a Code of Conduct incident happens to you, or you witness it happening to someone else, please contact the CoC team immediately, either in Discord or by sending an email to [conduct@feralsec.tv](mailto:conduct@feralsec.tv).

#### Guidelines for reporting incidents

Please do not feel like you may be a burden to us by reporting incidents. Even if you happen to report multiple incidents. We rather consider reports as an opportunity for us to act: by knowing about an incident, we can act on it, and often prevent it from going on or occuring again. But if we don't know, we can't take action.

If you're not sure if an incident was already reported by someone else, even if many people saw it, please report it. It is frequent that several people witness an incident and no one reports it, because everyone thought other people would.

If you are not sure whether the situation was a Code of Conduct violation, or whether it applied to that particular space, we encourage you to still report it. We would much rather have a few extra reports where we decide to take no action, rather than miss a report of an actual violation. We do not look negatively on you if we find the incident is not a violation. And knowing about incidents that are not violations, or happen outside our spaces, can also help us to improve the Code of Conduct or the processes surrounding it.

If you think you witness something, but you're not sure what you saw, or you don't have proof, it's still very useful to us if you can report on that. You don't have to build up a case before reporting, and it's ok to report things even if you have doubts.

In your report please include, when possible:

*   Your contact info (so we can get in touch with you)
    
*   Names (real, nicknames, or pseudonyms) of any individuals involved. If there were other witnesses besides you, please try to include them as well.
    
*   When and where the incident occurred. Please be as specific as possible.
    
*   Your account of what occurred. If there is a written record (e.g. tweets or slack messages), please include screenshots and, if possible, a link.
    
*   Any extra context relevant for the incident.
    
*   If this incident is ongoing.
    
*   Any other information you would like to give us.
    

If you don't have some of this information, or not at this time, please still make the report anyways. You can contact us at any time after your report if you want to add, edit or take back any information you shared.

#### Code of Conduct Active Response Ensurers

FeralSec does not have a dedicated Code of Conduct Active Response Ensurers (CARE) team and as such the FeralSec staff are in charge of handling reports.

If you feel unsafe directly reporting or wish to report a member of staff, you may choose someone to represent you. In this case, we'd need their contact information, but we'd ask this person to make clear that they are not reporting in their own name.

* * *

This code of conduct has been influenced by the following resources:

*   The Enforcement Guidelines were adapted from the [Contributor Covenant](https://www.contributor-covenant.org), version 2.0, available at [https://www.contributor-covenant.org/version/2/0/code\_of\_conduct.html](https://www.contributor-covenant.org/version/2/0/code_of_conduct.html).
    
*   This [anti-harassment policy](https://geekfeminism.fandom.com/wiki/Community_anti-harassment) is based on the example policy from the Geek Feminism wiki, created by the Geek Feminism community.
    
*   The [Djangocon.eu](https://djangocon.eu) Code of Conduct [https://2022.djangocon.eu/conduct/code\_of\_conduct/](https://2022.djangocon.eu/conduct/code_of_conduct/).
    
*   The Hackyderm [Code of Conduct / Rule Explainer](https://community.hachyderm.io/docs/rule-explainer/).
